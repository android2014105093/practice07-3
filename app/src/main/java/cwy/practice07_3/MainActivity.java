package cwy.practice07_3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnSelect = (Button)this.findViewById(R.id.btnSelect);
        Button btnInsert = (Button)this.findViewById(R.id.btnInsert);
        Button btnDelete = (Button)this.findViewById(R.id.btnDelete);
        Button btnCustom = (Button)this.findViewById(R.id.btnCustom);

        btnSelect.setOnClickListener(this);
        btnInsert.setOnClickListener(this);
        btnDelete.setOnClickListener(this);
        btnCustom.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent i;
        switch(v.getId()) {
            case R.id.btnSelect:
                i = new Intent(this, ListAct.class);
                startActivity(i);
                break;
            case R.id.btnInsert:
                i = new Intent(this, InsAct.class);
                startActivity(i);
                break;
            case R.id.btnDelete:
                i = new Intent(this, DelAct.class);
                startActivity(i);
                break;
            case R.id.btnCustom:
                i = new Intent(this, CusAct.class);
                startActivity(i);
                break;
        }
    }
}