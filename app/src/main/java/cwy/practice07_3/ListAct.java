package cwy.practice07_3;

import android.app.ListActivity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListAdapter;

public class ListAct extends ListActivity {

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        loadDB();
    }
    @Override
    public void onResume() {
        super.onResume();
        loadDB();
    }

    public void loadDB() {
        SQLiteDatabase db = openOrCreateDatabase (
                "test.db",
                SQLiteDatabase.CREATE_IF_NECESSARY,
                null
        );
        db.execSQL("CREATE TABLE IF NOT EXISTS people " +
                "(_id INTEGER PRIMARY KEY AUTOINCREMENT , name TEXT, age INTEGER);");
        Cursor c = db.rawQuery("SELECT * FROM people;", null);
        startManagingCursor (c);
        ListAdapter adapt = new SimpleCursorAdapter(
                this,
                android.R.layout.simple_list_item_2,
                c,
                new String[] {"name","age"},
                new int[] {android.R.id.text1,android.R.id.text2}, 0
        );
        setListAdapter(adapt);
        if (db != null) {
            db.close();
        }
    }
}
